package com.turvo.flashsale.service;

import java.time.LocalDateTime;
import java.util.Optional;

import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;

import com.turvo.flashsale.entity.RegisteredUser;
import com.turvo.flashsale.repository.RegisteredUserRepository;
import com.turvo.flashsale.util.ApplicationException;

public class RegisterUserServiceTest {

	@Mock
	RegisteredUserRepository repository;

	RegisterUserService service;

	String userId = "abcd";

	@Before
	public void setUp() {
		MockitoAnnotations.initMocks(this);
		service = new RegisterUserService();
		service.setRepository(repository);
		
	}

	@Test(expected = ApplicationException.class)
	public void testExecuteCheckUserAlreadyRegistered() {
		Mockito.when(repository.findById(userId)).thenReturn(Optional.of(new RegisteredUser(userId, LocalDateTime.now())));
		service.execute(userId);
	}

}
