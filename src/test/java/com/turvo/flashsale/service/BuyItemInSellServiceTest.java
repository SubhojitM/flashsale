package com.turvo.flashsale.service;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import java.time.LocalDateTime;
import java.util.HashMap;
import java.util.Map;
import java.util.Optional;

import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;

import com.turvo.flashsale.entity.Item;
import com.turvo.flashsale.entity.RegisteredUser;
import com.turvo.flashsale.repository.ItemRepository;
import com.turvo.flashsale.repository.OrderRepository;
import com.turvo.flashsale.repository.RegisteredUserRepository;
import com.turvo.flashsale.util.ApplicationException;

public class BuyItemInSellServiceTest {

	@Mock
	ItemRepository itemRepository;

	@Mock
	OrderRepository orderRepository;

	@Mock
	RegisteredUserRepository userRepository;
	
	
	Item orderItem;

	BuyItemInSellService service;

	String userId = "abcd";

	String itemId = "item1";

	@Before
	public void setUp() {
		MockitoAnnotations.initMocks(this);
		service = new BuyItemInSellService();
		service.setOrderRepository(orderRepository);
		service.setUserRepository(userRepository);
		service.setItemRepository(itemRepository);
	}

	@Test(expected = ApplicationException.class)
	public void testExecuteCheckUserUnregistered() {
		Mockito.when(userRepository.findById(userId)).thenReturn(Optional.empty());
		service.execute(userId, itemId);
	}
	
	@Test(expected = ApplicationException.class)
	public void testExecuteCheckBeforeSaleStart() {
		orderItem = new Item(itemId, "abcd", 10, LocalDateTime.parse("2020-04-15T15:52:40.410"), LocalDateTime.parse("2020-04-17T15:52:40.410"), LocalDateTime.now());
		Optional<Item> item = Optional.of(orderItem);
		Mockito.when(userRepository.findById(userId)).thenReturn(Optional.of(new RegisteredUser(userId, LocalDateTime.now())));
		Mockito.when(itemRepository.findById(itemId)).thenReturn(item);
		service.execute(userId, itemId);
	}
	
	@Test(expected = ApplicationException.class)
	public void testExecuteCheckAfterSaleEnd() {
		orderItem = new Item(itemId, "abcd", 10, LocalDateTime.parse("2020-04-15T15:52:40.410"), LocalDateTime.parse("2020-04-17T15:52:40.410"), LocalDateTime.now());
		Optional<Item> item = Optional.of(orderItem);
		Mockito.when(userRepository.findById(userId)).thenReturn(Optional.of(new RegisteredUser(userId, LocalDateTime.now())));
		Mockito.when(itemRepository.findById(itemId)).thenReturn(item);
		service.execute(userId, itemId);
	}
	
	
	@Test(expected = ApplicationException.class)
	public void testExecuteCheckItemNotInSale() {
		Optional<Item> item = Optional.empty();
		Mockito.when(userRepository.findById(userId)).thenReturn(Optional.of(new RegisteredUser(userId, LocalDateTime.now())));
		Mockito.when(itemRepository.findById(itemId)).thenReturn(item);
		service.execute(userId, itemId);
	}
	
	
	@Test
	public void executeSuccessfull() {
		orderItem = new Item(itemId, "abcd", 10, LocalDateTime.parse("2015-04-15T15:52:40.410"), LocalDateTime.parse("2020-04-17T15:52:40.410"), LocalDateTime.now());
		Optional<Item> item = Optional.of(orderItem);
		Mockito.when(userRepository.findById(userId)).thenReturn(Optional.of(new RegisteredUser(userId, LocalDateTime.now())));
		Mockito.when(itemRepository.findById(itemId)).thenReturn(item);
		Mockito.when(orderRepository.countByCreatedOnBetweenAndItem(item.get().getSaleStartOn(),item.get().getSaleEndOn(),item.get())).thenReturn(1l);
		service.execute(userId, itemId);
		assertTrue(orderItem.getCountInSale().intValue()>1);
	}
	

}
