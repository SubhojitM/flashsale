package com.turvo.flashsale.service;

import java.time.LocalDateTime;
import java.util.Optional;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.turvo.flashsale.entity.Item;
import com.turvo.flashsale.entity.Order;
import com.turvo.flashsale.entity.RegisteredUser;
import com.turvo.flashsale.repository.ItemRepository;
import com.turvo.flashsale.repository.OrderRepository;
import com.turvo.flashsale.repository.RegisteredUserRepository;
import com.turvo.flashsale.util.ApplicationException;

import lombok.Setter;

@Service
@Setter
public class BuyItemInSellService {

	@Transactional
	public Order execute(String userId, String itemId) {
		LocalDateTime buyTime  = LocalDateTime.now();		
		Optional<RegisteredUser> optional = userRepository.findById(userId);
		if(!optional.isPresent())
			throw new ApplicationException("User is not registered for the sale");
		Optional<Item> item = itemRepository.findById(itemId);
		if(!item.isPresent()) 
			throw new ApplicationException("Item: "+itemId+" is not in sale");		
		if(buyTime.isBefore(item.get().getSaleStartOn())) 
			throw new ApplicationException("Sale Not Started");
		if(buyTime.isAfter(item.get().getSaleEndOn())) 
			throw new ApplicationException("Sale Ended");		
		long count = orderRepository.countByCreatedOnBetweenAndItem(item.get().getSaleStartOn(),item.get().getSaleEndOn(),item.get());
		if(count >= item.get().getCountInSale()) 
			throw new ApplicationException("Item: "+itemId+" is sold out. Not available for sale");		
		Order order = new Order(userRepository.findById(userId).get(), item.get());
		return orderRepository.save(order);
					
	}
	
	@Autowired
	OrderRepository orderRepository;
	@Autowired
	RegisteredUserRepository userRepository;
	@Autowired
	ItemRepository itemRepository;
}
