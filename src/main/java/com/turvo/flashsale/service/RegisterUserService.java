package com.turvo.flashsale.service;

import java.time.LocalDateTime;
import java.util.Optional;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.turvo.flashsale.entity.RegisteredUser;
import com.turvo.flashsale.repository.RegisteredUserRepository;
import com.turvo.flashsale.util.ApplicationException;

import lombok.Setter;

@Service
@Setter
public class RegisterUserService {

	@Transactional
	public RegisteredUser execute(String userId) {
		Optional<RegisteredUser> optional = repository.findById(userId);
		if(optional.isPresent())
			throw new ApplicationException("User already registered");
		return repository.save(new RegisteredUser(userId, LocalDateTime.now()));
	}
	
	@Autowired
	RegisteredUserRepository repository;
}
