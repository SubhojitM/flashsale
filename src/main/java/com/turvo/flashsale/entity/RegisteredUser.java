package com.turvo.flashsale.entity;


import java.io.Serializable;
import java.time.LocalDateTime;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@Entity
@Table(name="registered_user", schema="flash_sale")
@ToString
@Setter
@Getter
@NoArgsConstructor
public class RegisteredUser implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = -8939090263021608544L;
	@Id
	@Column(name = "user_id", unique = true, nullable = false)
	private String userId;
	private LocalDateTime createdOn;
	
	public RegisteredUser(String userId, LocalDateTime createdOn) {
		super();
		this.userId = userId;
		this.createdOn = createdOn;
	}

}
