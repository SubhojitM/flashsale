package com.turvo.flashsale.entity;

import java.io.Serializable;
import java.time.LocalDateTime;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import com.turvo.flashsale.entity.RegisteredUser;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@Entity
@Table(name="order_detail", schema="flash_sale")
@ToString
@Setter
@Getter
@NoArgsConstructor
public class Order implements Serializable{
	/**
	 * 
	 */
	private static final long serialVersionUID = -9009316671427360636L;
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "order_id", unique = true, nullable = false)
	private Integer orderId;
	private LocalDateTime createdOn;
	
	@OneToOne
	@JoinColumn(name = "user_id")
	private RegisteredUser registeredUser;
	
	@OneToOne
	@JoinColumn(name = "item_id")
	private Item item;
	
	
	public Order(RegisteredUser registeredUser, Item item) {
		super();
		this.registeredUser = registeredUser;
		this.item = item;
	}	
	
}
