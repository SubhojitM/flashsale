package com.turvo.flashsale.entity;

import java.io.Serializable;
import java.time.LocalDateTime;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@Entity
@Table(name="item", schema="flash_sale")
@Getter
@Setter
@ToString
@NoArgsConstructor
@AllArgsConstructor
public class Item implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = -8664358565748142837L;
    
	@Id
	@Column(name = "item_id", unique = true, nullable = false)
	private String itemId;
	private String itemDescription;
	private Integer countInSale;
	private LocalDateTime saleStartOn;
	private LocalDateTime saleEndOn;
	private LocalDateTime createdOn;
	
}
