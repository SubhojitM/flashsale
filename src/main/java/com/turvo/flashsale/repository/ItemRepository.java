package com.turvo.flashsale.repository;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.turvo.flashsale.entity.Item;

@Repository
public interface ItemRepository extends CrudRepository<Item,String>{

}
