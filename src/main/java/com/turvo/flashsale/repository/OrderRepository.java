package com.turvo.flashsale.repository;

import java.time.LocalDateTime;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.turvo.flashsale.entity.Item;
import com.turvo.flashsale.entity.Order;

@Repository
public interface OrderRepository extends CrudRepository<Order, Integer>{


	long countByCreatedOnBetweenAndItem(LocalDateTime startTime, LocalDateTime endTime, Item item );
}
