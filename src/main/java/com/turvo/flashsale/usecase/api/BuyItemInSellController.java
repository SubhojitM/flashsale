package com.turvo.flashsale.usecase.api;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.turvo.flashsale.entity.Order;
import com.turvo.flashsale.service.BuyItemInSellService;

import lombok.extern.slf4j.Slf4j;

@Slf4j
@RestController
@RequestMapping("/buy")
public class BuyItemInSellController {

	@PostMapping
	public Order execute(@RequestParam("userId") String userId, @RequestParam("itemId") String itemId) {
		log.debug("executing service for buy an item for an user");		
		return service.execute(userId,itemId);
	}
	
	@Autowired
	BuyItemInSellService service;

}
