package com.turvo.flashsale.usecase.api;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.turvo.flashsale.entity.RegisteredUser;
import com.turvo.flashsale.service.RegisterUserService;

import lombok.extern.slf4j.Slf4j;

@Slf4j
@RestController
@RequestMapping("/register")
public class RegisterUserController {

	@PostMapping
	public RegisteredUser execute(@RequestParam("userId") String userId) {
		log.debug("executing service for registering an user");
		return service.execute(userId);
	}
	
	@Autowired
	RegisterUserService service;
}
