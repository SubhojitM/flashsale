package com.turvo.flashsale;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class TurvoFlashSalesAPIApplication {

	public static void main(String[] args) {
		SpringApplication.run(TurvoFlashSalesAPIApplication.class, args);
	}

}
