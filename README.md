This is a assignment project fro Turvo

# Flash Sale

## Tech Stack:
* 1. Spring Boot - 2.1.3
* 2. MySQL - 5.7.21-0ubuntu0.16.04.1
* 3. Java -1.8

## Code setup
### Build
* Install mvn 3.8 or above
* Install Java 8 or above
* run in terminal 
```
mvn clean install
```
* Deploy code at localhost:8080 using terminal
```
mvn spring-boot:run

```
## ER Diagram
![Screenshot_from_2019-04-17_14-40-52](/uploads/8c2eb17c4ab05e0f8da4fddb4c0072e4/Screenshot_from_2019-04-17_14-40-52.png)

## Class Diagram
![flash](/uploads/77f86faf806a8d1d7e1f56c4f193205d/flash.jpg)

## API Design


1. **/registerUser?userId=? (Method: POST)**

- This API is to register a user for a sale.

2. **/buy?userId=?&itemId=? (Method: POST)**

- This API is to create an order for the user already registered.
   

# Assumptions
- Not checking if the user is already registered in e-commerce portal. Only storing the user Id for further buy reference.
- Number of items for each Item and sale start and end time is preconfigured in csv file. That can be modified based on requirement.

# Exceptions
- If already registered the API will throw an error that  the user is already register for the sale.
- if the the buy time is not within the sale tenure.
- if the configured number of item already sold for an item.
- if the item does not exist for sale.
- if the user is not registered.


